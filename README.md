# README #

This Repository documents my solution to valaxy training Lambda project assignment. Solution is in Lambda_ValaxyProject.py

Details on the project requirement can be found here:
https://devopsrealtime.com/deploy-lambda-function-to-create-weekly-ec2-ami-backup/

### Other Weekly assignments ###

Other weekly assignment solution can be found here
https://github.com/yemisi/Valaxytraining.git