import boto3

ec2_client = boto3.client('ec2')

def create_ami():
    image = ec2_client.create_image(
        Description='test description for the server' + "testing",
        DryRun=False,
        InstanceId='i-09de06db2eb5c20b7',
        Name='dpt-web-server',
        NoReboot=True,
        TagSpecifications=[
            {
                'ResourceType': 'image',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': 'dpt-web-server-imagename'
                    },
                ]
            },
            {
                'ResourceType': 'snapshot',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': 'dpt-web-server-snapshotname'
                    },
                ]
            },        
        ]
    )
    return image

image = create_ami()

#print(image['ResponseMetadata']['HTTPStatusCode'])
if (image['ResponseMetadata']['HTTPStatusCode']) == 200:
    print(image)

