import boto3
import datetime
from dateutil.parser import parse
import botocore

ec2_client = boto3.client('ec2',region_name='ca-central-1')
ec2_response = ec2_client.describe_instances()
age_days = 1

#get all ami ids created by me
def get_all_ami_ids():
    ami_ids = set()
    images = ec2_client.describe_images(Owners=['self',])
    for ami_id in images['Images']:
      ami_ids.add(ami_id['ImageId'])
    return ami_ids

#get all ami ids currently used by non-terminated instances
def get_instance_ami_ids(ec2_response):
    instance_amis = set()
    for reservation in (ec2_response['Reservations']):
        for instance_id in reservation['Instances']:
            if instance_id['State']['Name'] == 'stopped' or instance_id['State']['Name'] == 'running':
                instance_amis.add(instance_id['ImageId'])
    return instance_amis

#get a list/set of unused ami ids 
def get_unused_ami_ids(all_ami,used_ami):
    return all_ami - used_ami


def cleanup_unused_ami(unused_ami):
    for image_id in unused_ami:
        a = ec2_client.describe_images(ImageIds=[image_id])['Images'][0]['CreationDate']
        day_delta = datetime.datetime.now().date() - parse(a).replace(tzinfo=None).date()
        try:
            if day_delta.days <= age_days:
                image_dereg_resp = ec2_client.deregister_image(ImageId=image_id)
                if image_dereg_resp['ResponseMetadata']['HTTPStatusCode'] == 200:
                    print("Deleted ami: " + image_id)
        except botocore.exceptions.ClientError as error:
            print(error.response['Error']['Code'] + " exception occured while deleting ami: " + image_id)
        continue

def cleanup_unused_snapshot(snapshots):
    for snapshot in snapshots['Snapshots']:
        day_delta = datetime.datetime.now().date() - snapshot['StartTime'].date()
        try:
            if day_delta.days <= age_days:
                delete_response = ec2_client.delete_snapshot(SnapshotId=snapshot['SnapshotId'])
                if delete_response['ResponseMetadata']['HTTPStatusCode'] == 200:
                     print("Deleted snapshot: " + snapshot['SnapshotId'])
        except botocore.exceptions.ClientError as error:
                if error.response['Error']['Code'] == 'InvalidSnapshot.InUse':
                    print("Skipped this snapshot which is in use: " + snapshot['SnapshotId'] )
                else:
                    print(error.response['Error']['Code'] + " exception error occured while deleting " + snapshot['SnapshotId'])
        continue


unused_ami_ids = get_unused_ami_ids(get_all_ami_ids(),get_instance_ami_ids(ec2_response))
cleanup_unused_ami(unused_ami_ids)
#snapshots = ec2_client.describe_snapshots(OwnerIds=['self'])
cleanup_unused_snapshot(ec2_client.describe_snapshots(OwnerIds=['self']))



