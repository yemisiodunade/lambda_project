import boto3
import datetime
from dateutil.parser import parse
import botocore


ec2_client = boto3.client('ec2',region_name='ca-central-1')
# ec2_client = boto3.client('ec2',region_name='us-east-1')
ec2_response = ec2_client.describe_instances()
age_days = 1
#snapshot_ids = set()


def get_all_amis():
    ami_ids = set()
    images = ec2_client.describe_images(Owners=['self',])
    for ami_id in images['Images']:
      ami_ids.add(ami_id['ImageId'])
    return ami_ids

def get_instance_amis(ec2_response):
    instance_amis = set()
    for reservation in (ec2_response['Reservations']):
        for instance_id in reservation['Instances']:
            instance_amis.add(instance_id['ImageId'])
    return instance_amis

def get_unused_amis(all_ami,used_ami):
    # If A and B are two sets. The set difference of A and B is a set of elements that exists only in set A but not in B. For example:
    return all_ami - used_ami


usedami = get_instance_amis(ec2_response)
# print("used_ami: ")
# print(usedami)

allami = get_all_amis()
# print('all amis: ')
# print(allami)

unused_ami = get_unused_amis(allami,usedami)
# print("all unused amis: ")
# print(unused_ami)

for image_id in unused_ami:
       a = ec2_client.describe_images(ImageIds=[image_id])['Images'][0]['CreationDate']
    #    print(a)
    #    b = parse(a).replace(tzinfo=None).date()
    #    print(b)
    #    c = datetime.datetime.now().date()
    #    print(c)
    #    d = c - b
    #    print(d)
       day_delta = datetime.datetime.now().date() - parse(a).replace(tzinfo=None).date()
       try:
        if day_delta.days <= age_days:
            print("Deleting ami: " + image_id)
            image_dereg_resp = ec2_client.deregister_image(ImageId=image_id)
       except botocore.exceptions.ClientError as error:
           raise error
       continue
           

snapshots = ec2_client.describe_snapshots(OwnerIds=['self'])
for snapshot in snapshots['Snapshots']:
       #a = snapshot['StartTime']
    #    b = snapshot['StartTime'].date()
    #    c = datetime.datetime.now().date()
    #    d = c - b
       day_delta = datetime.datetime.now().date() - snapshot['StartTime'].date()
       try:
        if day_delta.days <= age_days:
           print("Deleting snapshot: " + snapshot['SnapshotId'])
           ec2_client.delete_snapshot(SnapshotId=snapshot['SnapshotId'])
       except botocore.exceptions.ClientError as error:
            if error.response['Error']['Code'] == 'InvalidSnapshot.InUse':
              print("skipping this snapshot which is in use: " + snapshot['SnapshotId'] )
            else:
               raise error
       continue


#http://www.opscode.in/blog/unused-ami-cleanup#the-final-cleanup
#https://gist.github.com/nirbhabbarat/ee8772b7ba677fc45dc4ec4d26e4b19d
#https://hands-on.cloud/working-with-snapshots-and-amis-using-boto3-in-python/#How-to-list-EBS-volume-snapshots-using-Boto3
#https://boto3.amazonaws.com/v1/documentation/api/latest/guide/error-handling.html
#http://www.opscode.in/blog/unused-ami-cleanup#the-final-cleanup
#https://www.geeksforgeeks.org/python-datetime-timedelta-function/
#https://stackoverflow.com/questions/48124269/python-boto3-program-to-delete-old-snapshots-in-aws

