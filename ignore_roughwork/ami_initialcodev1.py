import boto3
from botocore.config import Config
from datetime import datetime
import sys

# ec2_region = 'us-east-1'
ec2_region = 'ca-central-1'
amis = []

# my_config = Config(
#     region_name = ec2_region,
#     signature_version = 'v4',
#     retries = {
#         'max_attempts': 10,
#         'mode': 'standard'
#     }
# )


#ec2_client = boto3.client('ec2',config=my_config)
ec2_client = boto3.client('ec2',region_name=ec2_region)
#account for condition of no stopped or running instance
ec2_response = ec2_client.describe_instances() 
#print(ec2_response)

#Exit if there are no instances to capture snapshots from
def check_running_stopped():
    count  = 0
    for reservation in (ec2_response['Reservations']):
        for instances in reservation['Instances']:
            if instances['State']['Name'] == 'stopped' or instances['State']['Name'] == 'running':
                        count+= 1
                        # print(count)
    if count == 0:
        print("There are no instances running or stopped")
        sys.exit()



#print(response['Reservations'][0]['Instances'][0]['InstanceId'])
def get_instances_tags(response):
    ids_tags = {}
    for reservation in (response['Reservations']):
        for instance_id in reservation['Instances']:
            #print(instance_id['InstanceId'])
            print(instance_id['Tags'])
            for tags in instance_id['Tags']:
                if tags['Key'] == 'Name':
                    #print(tags['Value'])
                  ids_tags[instance_id['InstanceId']] = tags['Value']
    return ids_tags


def timestamp():
    return "_" + datetime.today().strftime('%Y-%m-%d-%H_%M_%S')

def create_ami(instance_id,tag_value):
    image = ec2_client.create_image(
        Description='This is ami for ' + instance_id,
        DryRun=False,
        InstanceId=instance_id,
        Name=tag_value + timestamp(),
        NoReboot=True,
        TagSpecifications=[
            {
                'ResourceType': 'image',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': tag_value
                    },
                ]
            },
            {
                'ResourceType': 'snapshot',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': tag_value
                    },
                ]
            },        
        ]
    )
    return image


check_running_stopped()
instanceids_tags = get_instances_tags(ec2_response)
# #create amis per requirement
for id, tags in instanceids_tags.items():
   image = create_ami(id,tags)
   print(image)
